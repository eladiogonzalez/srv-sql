package com.example.demo.facturas;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/apitechu/v2")
public class FacturasController {
    @Autowired FacturasRepository facturasRepository;

    @GetMapping("/facturas")
    public Iterable<FacturaModel> getFacturas() {
        return facturasRepository.findAll();
    }

}
