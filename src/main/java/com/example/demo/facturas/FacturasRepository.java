package com.example.demo.facturas;

import org.springframework.data.repository.CrudRepository;

public interface FacturasRepository extends CrudRepository<FacturaModel, Integer> {

}
